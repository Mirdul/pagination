$(function () {
    $("#signupForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            name: {
                required: true,
                minlength: 3
            },
            message: {
                required: true,
                minlength: 10
            },
            date: "required"
        },
        messages: {
            email: "Please enter a valid email address",
            name: {
                required: "Please enter your name",
                minlength: "Your name must be at least 3 characters long"
            },
            message: {
                required: "Please enter any comment",
                minlength: "Your comment must be at least 10 characters long",
            },
            date: "Please select any date"
        }
    });
});
$(function () {
    $("#button").click(function () {
        $("#submit").submit();
    });
});
$.validator.setDefaults({
    submitHandler: function () {
        var name = $("#name").val();
        var email = $("#email").val();
        var message = $("#message").val();
        var date = $("#date").val();
        $.ajax({
            url: "insert.php",
            type: "POST",
            data: { name: name, email: email, message: message, date: date },
            success: function (data) {
                $("form").trigger("reset"),
                    $("#msg").fadeIn().html(data);
                setTimeout(function () {
                    $("#msg").fadeOut('slow');
                }, 2000);
                setTimeout(function () {
                    $("#myModal").modal('hide');
                }, 5000);
                $.ajax({
                    url: 'data.php',
                    type: 'POST',
                    dataType: 'json',
                    cache: false,
                    success: function (data) {
                        var emp_data = "";
                        $.each(data, function (index, value) {
                            emp_data += '<tr>';
                            emp_data += '<td>' + value.name + '</td>';
                            emp_data += '<td>' + value.email + '</td>';
                            emp_data += '<td>' + value.message + '</td>';
                            emp_data += '<td>' + value.date + '</td>';
                            emp_data += '</tr>';
                        });
                        $('tbody').html(emp_data);
                    }
                });
            }
        });

    }
});
$(function () {
    var sel = 10;
    $.ajax({
        url: 'data.php',
        type: 'POST',
        dataType: 'json',
        cache: false,
        success: function (data) {
            var emp_data = "";
            $.each(data, function (index, value) {
                emp_data += '<tr>';
                emp_data += '<td>' + value.name + '</td>';
                emp_data += '<td>' + value.email + '</td>';
                emp_data += '<td>' + value.message + '</td>';
                emp_data += '<td>' + value.date + '</td>';
                emp_data += '</tr>';
            });
            $('tbody').html(emp_data);
        }
    });
    $.ajax({
        url: 'page.php',
        type: 'POST',
        dataType: 'json',
        cache: false,
        success: function (data) {
            var result = data / sel;
            var final = Math.ceil(result);
            var page = "";
            page += '<ul class="pagination right">';
            //page += '<li class="page-item"><a class="page-link page" href ="#" id="pre">Previous</a ></li >';
            for (var index = 1; index <= final; index++) {
                page += '<li class="page-item"><a class="page-link page" href ="#" id=' + index + '>' + index + '</a></li>';

            }
            //page += '<li class="page-item"><a class="page-link page" href ="#" id="next">Next</a ></li >';
            page += '</ul>';
            $('#page').html(page);
        }
    });
});
$(function () {
    $(document).on('click', '.sort', function () {
        var sel = $("#sel option:selected").val();
        var column_name = $(this).attr("id");
        var order = $(this).data("order");
        var arrow = '';
        $("#test-order").val(order);
        $("#test-col").val(column_name);
        if (order == 'asc') {
            arrow = '<a href="#" class="sort" data-order="desc"  id=' + column_name + '>' + column_name + '</a><span class="glyphicon glyphicon-arrow-down"></span>';
        }
        else {
            arrow = '<a href="#" class="sort" data-order="asc"  id=' + column_name + '>' + column_name + '</a><span class="glyphicon glyphicon-arrow-up"></span>';

        }
        $.ajax({
            url: 'sort.php',
            type: 'post',
            data: { column_name: column_name, order: order, sel: sel },
            dataType: 'json',
            cache: false,
            success: function (data) {
                var emp_data = "";
                $.each(data, function (index, value) {
                    emp_data += '<tr>';
                    emp_data += '<td>' + value.name + '</td>';
                    emp_data += '<td>' + value.email + '</td>';
                    emp_data += '<td>' + value.message + '</td>';
                    emp_data += '<td>' + value.date + '</td>';
                    emp_data += '</tr>';
                });
                $('tbody').html(emp_data);
                $("th[name=" + column_name + "]").html(arrow);
            }
        });
    });
});
$(function () {
    $(document).on('click', '.page', function () {
        var num = $(this).attr("id");
        var sel = $("#sel option:selected").val();
        var col = $("#test-col").val();
        var order = $("#test-order").val();
        $.ajax({
            url: 'pagination.php',
            type: 'POST',
            data: { sel: sel, num: num, col: col, order: order },
            dataType: 'json',
            cache: false,
            success: function (data) {
                var emp_data = "";
                $.each(data, function (index, value) {
                    emp_data += '<tr>';
                    emp_data += '<td>' + value.name + '</td>';
                    emp_data += '<td>' + value.email + '</td>';
                    emp_data += '<td>' + value.message + '</td>';
                    emp_data += '<td>' + value.date + '</td>';
                    emp_data += '</tr>';
                });
                $('tbody').html(emp_data);

            }
        });
    });
});
